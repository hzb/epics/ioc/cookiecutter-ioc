#!/bin/bash

# Set locale to avoid warnings
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

# Source the IOC path configuration file that contains IOC_ST_CMD_PATH
source /opt/epics/ioc_path.txt

KIND=${KIND:-general}  # Set kind to 'general' if not already defined

# Check if the environment variable IOC_ST_CMD_PATH is set
if [ -n "$IOC_ST_CMD_PATH" ]; then
    # Print the IOC path for debugging
    echo "IOC_ST_CMD_PATH is set to: $IOC_ST_CMD_PATH"
    
    # Check if deploy.yml exists for motor configuration
    if [ "$KIND" != "general" ]; then
        echo "deploy.yml exists, $KIND"
        # Generate motor configuration using the epics_motor_gen tool
        /opt/epics/venv/bin/gen_epics_motor --output_file_path $IOC_ST_CMD_PATH /opt/epics/deploy.yml $IOC_NAME   
    else
        # Notify if deploy.yml is missing but may not be needed
        echo "deploy.yml does not exist or is set to general, motor configuration was not created. If you're running a non-motor IOC, then ignore this message."
    fi
    
    # Change to the IOC directory, exit with error if cd fails
    cd "$IOC_ST_CMD_PATH" || { echo "Failed to change directory to $IOC_ST_CMD_PATH"; exit 1; }
else
    # Exit if IOC path is not set since it's required
    echo "IOC_ST_CMD_PATH is not set, IOC will not start"
    exit 1
fi

# use specified user name or use `default` if not specified
MY_USERNAME="${MY_USERNAME:-default}"

# use specified group name or use the same user name also as the group name
MY_GROUP="${MY_GROUP:-${MY_USERNAME}}"

# use the specified UID for the user
MY_UIDD="${MY_UIDD:-1000}"

# use the specified GID for the user
MY_GIDD="${MY_GIDD:-${MY_UIDD}}"


# check to see if group exists; if not, create it
if getent group "${MY_GROUP}" > /dev/null 2>&1
then
  echo "INFO: Group exists; skipping creation"
else
  echo "INFO: Group doesn't exist; creating..."
  # create the group
  addgroup --gid "${MY_GIDD}" "${MY_GROUP}" || (echo "INFO: Group exists but with a different name; renaming..."; groupmod --gid "${MY_GIDD}" -n "${MY_GROUP}" "$(getent group | awk -F ':' -v gid="${MY_GIDD}" '$3==gid {print $1}')")
fi


# check to see if user exists; if not, create it
if id -u "${MY_USERNAME}" > /dev/null 2>&1
then
  echo "INFO: User exists; skipping creation"
else
  echo "INFO: User doesn't exist; creating..."
  # create the user without prompts for additional information
  useradd --uid "${MY_UIDD}" --gid "${MY_GIDD}" --home "/home/${MY_USERNAME}" --shell /bin/sh --no-create-home --system "${MY_USERNAME}"
  # Set the password to be disabled
  passwd -l "${MY_USERNAME}"  # Lock the password to disable login
fi

# make the directories needed to run my app
mkdir -p /opt/epics

# change ownership of any directories needed to run my app as the proper UID/GID
chown -R "${MY_USERNAME}:${MY_GROUP}" "/opt/epics"

# start myapp
echo "INFO: Running epics as ${MY_USERNAME}:${MY_GROUP} (${MY_UIDD}:${MY_GIDD})"

# # exec and run the actual process specified in the CMD of the Dockerfile (which gets passed as ${*})
# exec gosu "${MY_USERNAME}" "${@}"

# Execute the command passed to the script with its arguments
# This allows for interactive shell access in the IOC directory
exec "$@"

